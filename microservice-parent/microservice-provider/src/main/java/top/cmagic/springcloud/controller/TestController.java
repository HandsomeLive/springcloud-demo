package top.cmagic.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.cmagic.springcloud.api.entity.User;
import top.cmagic.springcloud.api.service.TestService;


/**
 * @Description:
 * @Author: WangShiCheng
 * @Date: Created on 2018/2/10.
 */

@RestController
public class TestController implements TestService {

    @GetMapping("/login.do")
    public User getUser() {
        User user = new User();
        user.setUserName("zhangsan");
        user.setPassword("123456");
        return user;
    }
}

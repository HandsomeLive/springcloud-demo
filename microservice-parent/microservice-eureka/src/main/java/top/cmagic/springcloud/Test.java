package top.cmagic.springcloud;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author wangsc
 * @date: 2020年04月17日 22:28
 * @since JDK 1.8
 */
public class Test implements ApplicationContextAware {
    static boolean foo(char c) {
        System.out.print(c);
        return true;
    }

    private static Map<String, Object> serviceBeans;

    private static ApplicationContext applicationContext;

    public static void main(String[] argv) {


//        int[] a={4,2,3,1};
//        List<String> str=new ArrayList<>();
//        for (int i=0;i<a.length;i++){
//            str.add(String.valueOf(a[i]));
//        }
//        Collections.sort(str);
//        String result="";
//        for (String l:str){
//            result+=l;
//        }
//        int value = Integer.valueOf(result);
//        System.out.println(value);

//        String s1 = "abc";
//        String s2 = s1;
//        String s3 = "abc";
//        String s4 = new String("abc");
//        String s5 = new String("abc");
//        System.out.println("1: "+(s1 == s2));
//        System.out.println("2: "+(s1 == s3));
//        System.out.println("3: "+s1.equals(s2));
//        System.out.println("4: "+s4 == s5);
//        System.out.println("5: "+s4.equals(s5));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.serviceBeans = applicationContext.getBeansWithAnnotation(Service.class);
        this.applicationContext=applicationContext;
    }

    public static <T> List<T> getBeansByInterface(Class<T> interfaceType) {
        List<T> tList = new ArrayList<T>();
        for (String key : serviceBeans.keySet()) {
            Object obj = serviceBeans.get(key);
            for (Class<?> c : obj.getClass().getInterfaces()) {
                if (c.getName().equals(interfaceType.getName())) {
                    tList.add((T) obj);
                }
            }
        }
        return tList;
    }
}
package top.cmagic.springcloud.api.service;

import org.springframework.web.bind.annotation.GetMapping;
import top.cmagic.springcloud.api.entity.User;

/**
 * @author wsc
 * @date: 2019年11月10日 19:49
 * @since JDK 1.8
 */
public interface TestService {

    @GetMapping("/login.do")
    public User getUser();

}

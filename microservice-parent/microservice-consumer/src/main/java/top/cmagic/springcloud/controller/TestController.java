package top.cmagic.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import top.cmagic.springcloud.api.entity.User;
import top.cmagic.springcloud.service.TestFeignService;

import javax.annotation.Resource;

/**
 * @Description:
 * @Author: WangShiCheng
 * @Date: Created on 2018/2/10.
 */

@RestController
public class TestController {

    @Resource
    private TestFeignService testFeignService;

    @GetMapping("/loginconsumer.do")
    public User getUser() {
        return testFeignService.getUser();
    }
}

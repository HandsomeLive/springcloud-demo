package top.cmagic.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import top.cmagic.springcloud.api.service.TestService;

/**
 * @author wsc
 * @date: 2019年11月10日 19:53
 * @since JDK 1.8
 */
@FeignClient(name = "cmagic-microservice-provider")
public interface TestFeignService extends TestService {
}
